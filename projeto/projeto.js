/* window.onload = function() {
	document.getElementById('btCadastrar').onclick = function() {
		alert(document.getElementById('txtNome').value);
	}
} */

class Produto {
	constructor(codigo, nome, unidade, valor, NCM) {
		this.codigo = codigo;
		this.nome = nome;
		this.unidade = unidade;
		this.valor = valor;
		this.NCM = NCM;
	}
}

var listaProdutos = [];
var posicao = '';

function cadastrar(lista, objeto) {
	lista.push(objeto);
}

function alterar(lista, objeto, pos) {
	lista[pos] = objeto;
}

function excluir(lista, pos) {
	lista.splice(pos, 1);
}

function listar(lista, tabela) {
	let auxHtml = '';
	for (let i = 0; i < lista.length; i++) {
		auxHtml += '<tr>'+
							 '<td>'+ lista[i].codigo +'</td>'+
							 '<td>'+ lista[i].nome +'</td>'+
							 '<td>'+ lista[i].unidade +'</td>'+
							 '<td>'+ lista[i].valor +'</td>'+
							 '<td>'+ lista[i].NCM +'</td>'+
							 '<td>'+
							 '<button class="btn btn-warning btAlterar" rel="'+ i +'">'+
							 	 'A'+
							 '</button>'+
							 '</td>'+
							 '<td>'+
							 '<button class="btn btn-danger btExcluir" rel="'+ i +'">'+
							 	 'X'+
							 '</button>'+
							 '</td>'+
							 '</tr>';
	}
	$(tabela).html(auxHtml);
	//document.getElementById(tabela).innerHTML = auxHtml;
}

$(document).ready(function() {
	$('#btSalvar').click(function() {
		// codigo = document.getElementById('txtCodigo').value;
		codigo = $('#txtCodigo').val();
		nome = $('#txtNome').val();
		unidade = $('#txtUnidade').val();
		valor = $('#txtValor').val();
		NCM = $ ('#txtNCM').val();
		produto = new Produto(codigo, nome, unidade, valor,NCM);
		if (posicao != '') {
			alterar(listaProdutos, produto, posicao);
			posicao = '';
		} else {
			cadastrar(listaProdutos, produto);
		}
		$('input').val('');
		listar(listaProdutos, '#tbodyTabelaProduto');
	});

	$(document).on('click', '.btAlterar', function() {
		posicao = $(this).attr('rel');
		$('#txtCodigo').val(listaProdutos[posicao].codigo);
		$('#txtNome').val(listaProdutos[posicao].nome);
		$('#txtUnidade').val(listaProdutos[posicao].unidade);
		$('#txtValor').val(listaProdutos[posicao].valor);
		$('#txtNCM').val(listaProdutos[posicao].NCM);
	});

	$(document).on('click', '.btExcluir', function() {
		let posicaoExcluir = $(this).attr('rel');
		if (confirm('Tem certeza que deseja excluir o produto?')) {
			excluir(listaProdutos, posicaoExcluir);
			listar(listaProdutos, '#tbodyTabelaProduto');
		}
	});

	$('#btAJAX').click(function() {
		$.ajax({
		  url: 'http://date.jsontest.com/',
		  method: 'GET'
		}).done(function(response) {
			$('#infoAJAX').html(response.date +' - '+ response.time);
		});		
	});

	$('#btJSON').click(function() {
		$('#infoJSON').html(JSON.stringify(listaProdutos));
	});
});